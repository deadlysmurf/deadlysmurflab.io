---
title: Margaret Anderson Kilfoil
subtitle: About Me
comments: false
---

I learned to knit as a small child, although it grew into more of a "lifestyle choice" than a hobby by my early 20s. 
Around this time, I also learned about to spin, crochet and weave, although knitting remains my true passion. 


The other thing you need to know about me is that I am a huge nerd. 
I am interested in both academic research about knitting, and the possibility of using my data science skills in the yarn community. 
So many people overlook the ways that data and mathematics are intertwined with fibre arts like knitting! 
Luckily, I’ve met a lot of other data lovers who knit, and who inspire me to try new things.
Having written my thesis on the sociology of gender in knitting, I also hope to have the opportunity to refresh this work someday, and give it the time it deserves. 
If you ever want to geek out over data stuff or sociology, or yarn, feel free to drop me a line.